//Armijos Cabrera Ariana
//Bustamante Quiridumbay Hillary
//Chiang Macias Yahir
//Carbo Vazquez Derek

package fechaNacimiento;
	
import java.util.List;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {
	
	private TextField nameTextField;
	private TextField edadTextField;
	
	@Override
    public void start(Stage primaryStage) {
        //Creacion del TextField
		nameTextField = new TextField();
        edadTextField = new TextField();
        
        
        Button button = new Button("Agregar");
        button.setOnAction(e -> Datos());

        VBox vbox = new VBox(10);
        vbox.setPadding(new Insets(10));
        vbox.getChildren().addAll(nameTextField, edadTextField, button);

        Scene scene = new Scene(vbox, 300, 200);

        primaryStage.setScene(scene);
        primaryStage.setTitle("JavaFX: TextField y DatePicker");

        primaryStage.show();
    }

    private void Datos() {
        String nombre = nameTextField.getText();
        String edad = edadTextField.getText();

        System.out.println("Nombre de Estudiante: " + nombre);
        System.out.println("Edad: " + edad);
    }

    public static void main(String[] args) {
        launch(args);
    }
}